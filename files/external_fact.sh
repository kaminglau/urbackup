#!/bin/bash

if [[ -f /etc/init.d/urbackup_srv ]]; then 
  /etc/init.d/urbackup_srv status > /dev/null 2>&1
  [ "$?" -eq "0" ] && echo "appcara_urbackup_ready=true" || echo "appcara_urbackup_ready=false"
elif [[ -f /etc/init.d/urbackup_client ]]; then
  /etc/init.d/urbackup_client status > /dev/null 2>&1
  [ "$?" -eq "0" ] && echo "appcara_urbackup_ready=true" || echo "appcara_urbackup_ready=false"
fi
