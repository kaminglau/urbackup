# This is a urbackup
class urbackup() {
  include urbackup::params

    if ($urbackup::params::server) and ($urbackup::params::server['recipes']['urbackup']) {
    class { 'urbackup::dependencies' : }
    class { 'urbackup::install' : }
  
    anchor { 'urbackup::begin' : } ->
      Class['urbackup::dependencies'] ->
      Class['urbackup::install'] ->
    anchor { 'urbackup::end' : }

    file { 'urbackup-fact':
      ensure => file,
      name   => '/etc/facter/facts.d/appcara_urbackup_ready.sh',
      source => 'puppet:///modules/urbackup/external_fact.sh',
      owner  => root,
      group  => root,
      mode   => '0755',
    }
  }
}
