# urbackup param file
class urbackup::params() {

  $server = $appcara::params::server

  if $server {

    $version = $server['recipes']['urbackup']['general'][0]['version']
    $package_repo = $::osfamily ? {
      'Debian' => 'ppa:uroni/urbackup',
      'RedHat' => 'urbackup/kot1grun.repo.erb',
      default  => undef,
    }
    $role = $server['recipes']['urbackup']['general'][0]['role']
    $client_download_url = "${appstack_repo_url}/urbackup/client"
    $client_install = $::kernel ? {
      'Linux'   => '/var/lib/appstack/tmp',
      'Windows' => 'C:\\ProgramData\\Appcara\\AppStack\\tmp',
      default   => fail('Unsupported OS to define client_install path.'),
    }
    $client_file_name = $::kernel ? {
      'Linux'   => "urbackup-client-${version}",
      'Windows' => "UrBackupClient${version}",
      default   => fail('Unsupported OS to define client_file_name.'),
    }
    $client_initfile = $::osfamily ? {
      'Debian' => 'urbackup/urbackup_client_debian.erb',
      'RedHat' => 'urbackup/urbackup_client_redhat.erb',
      default  => undef,
    }


    case $role {
      'server': {
        $urbackup_server_package = $osfamily ? {
          /(Debian|RedHat)/  => 'urbackup-server',
          default            => fail('Unsuported OS for UrBackup Server.'),
        }
      }
      'client': {
        $client_package = $::kernel ? {
          'Linux'   => "${client_file_name}.tar.gz",
          'Windows' => "${client_file_name}.exe",
          default   => fail('Unsuported OS for UrBackup Client.'),
        }
      }
      default: {
        fail("Undefined role for UrBackup system.")
      }
    }

  } # end if $server
}
