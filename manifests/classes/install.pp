# Class: urbackup::install
class urbackup::install() {
  include urbackup::params

  if $urbackup::params::server {
    case $urbackup::params::role {
      'server': {
        package { $urbackup::params::urbackup_server_package :
          ensure => installed,
        }
      }
      'client': {
        # Download the source package
        appcara::downloader::download { $urbackup::params::client_package :
          ensure      => present,
          url         => "${urbackup::params::client_download_url}/${urbackup::params::client_package}",
          download_to => $urbackup::params::client_install,
          timeout     => 0,
        }

        # Extract the source package
        appcara::downloader::extract { $urbackup::params::client_file_name :
          ensure     => present,
          extract_to => $urbackup::params::client_install,
          src_dir    => $urbackup::params::client_install,
          timeout    => 0,
          notify     => Exec['configure-urbackup'],
          require    => Appcara::Downloader::Download[$urbackup::params::client_package],
        }

        # Run configure source option
        exec { 'configure-urbackup':
          path    => $::path,
          command => "${urbackup::params::client_install}/${urbackup::params::client_file_name}/configure --enable-headless",
          cwd     => "${urbackup::params::client_install}/${urbackup::params::client_file_name}",
          creates => "${urbackup::params::client_install}/${urbackup::params::client_file_name}/Makefile",
          notify  => Exec['make-urbackup'],
        }

        # Run make to compile source
        exec { 'make-urbackup':
          path    => $::path,
          command => 'make',
          cwd     => "${urbackup::params::client_install}/${urbackup::params::client_file_name}",
          creates => "${urbackup::params::client_install}/${urbackup::params::client_file_name}/urbackup_client",
          notify  => File['init-urbackup'],
        }

        # Create init file
        file { 'init-urbackup':
          ensure  => file,
          path    => '/etc/init.d/urbackup_client',
          content => template($urbackup::params::client_initfile),
          mode    => '0755',
          owner   => 'root', group => 'root',
        }

        service { 'urbackup_client':
          ensure  => running,
          require => File['init-urbackup'],
        }
      }
      default: {
        notify { 'No installation role defined.': }
      }
    } # end case role
  } # end if server

}
