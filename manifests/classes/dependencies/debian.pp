# Dependencies for Debian based OS
class urbackup::dependencies::debian {
  include urbackup::params

  case $urbackup::params::role {
    'server': {
      apt::ppa { $urbackup::params::package_repo : }
    }
    'client': {
      include appcara::buildenv
      urbackup::package { 'g++': }
      urbackup::package { 'libcrypto++-dev': }
    }
    default: {
      notify { 'No installation role defined.': }
    }
  }
}
