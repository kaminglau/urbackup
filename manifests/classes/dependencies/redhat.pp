# Dependencies for Redhat based OS
class urbackup::dependencies::redhat {
  include urbackup::params

  case $urbackup::params::role {
    'server': {
      file { 'urbackup_repo' :
        ensure  => file,
        path    => '/etc/yum.repos.d/kot1grun.repo',
        content => template($urbackup::params::package_repo),
      }
    }
    'client': {
      include appcara::buildenv
      urbackup::package { 'gcc-c++': }
      urbackup::package { 'cryptopp-devel': }
    }
    default: {
      notify { 'No installation role defined.': }
    }
  }
}
