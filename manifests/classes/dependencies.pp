define urbackup::package {
  if ! defined(Package[$name]) {
    package { $name:
      ensure => installed
    }
  }
}

class urbackup::dependencies {
  include urbackup::params 

  case $::osfamily {
    Debian: { require urbackup::dependencies::debian }
    RedHat: { require urbackup::dependencies::redhat }
  }
}
