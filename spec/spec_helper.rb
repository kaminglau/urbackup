require 'debugger'
require 'puppetlabs_spec_helper/module_spec_helper'
require 'hiera-puppet-helper'

# hiera shared context
fixture_path = File.expand_path(File.join(__FILE__, '..', 'fixtures'))
shared_context 'hieradata' do
  let(:hiera_config) do
    {
      backends: %w{ rspec yaml },
      hierarchy: ['default'],
      yaml: { datadir: File.join(fixture_path, 'hieradata') },
      rspec: respond_to?(:hiera_data) ? hiera_data : {}
    }
  end
end

# shared examples for compile
shared_examples :compile, compile: true do
  it { should compile.with_all_deps }
end

# include all support files
Dir['./spec/support/**/*.rb'].each { |f| require f }

# configure RSpec
RSpec.configure do |c|
  c.mock_framework = :rspec
  c.color_enabled = true
  c.tty = true
  c.formatter = :documentation
  c.treat_symbols_as_metadata_keys_with_true_values = true
  c.default_facts = RSpec::CentOS.centos_facts.merge(RSpec::AppStackFacts.appstack_facts)
  c.before(:each) do
    Puppet::Util::Log.level = :warning
    Puppet::Util::Log.newdestination(:console)
    # work around https://tickets.puppetlabs.com/browse/PUP-1547
    # ensure that there's at least one provider available by emulating that any command exists
    require 'puppet/confine/exists'
    Puppet::Confine::Exists.any_instance.stubs(which: '')
    # avoid "Only root can execute commands as other users"
    Puppet.features.stubs(:root? => true)
  end
end
