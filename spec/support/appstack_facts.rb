# common facts for AppStack
require 'rspec/core/shared_context'

# we need a module to have a default shared_context, we can't use include_context from RSpec.configure
module RSpec
  # AppStack
  module AppStackFacts
    extend RSpec::Core::SharedContext

    def self.appstack_facts
      {
        appstack_server_id: '1',
        appstack_tier_id: '1',
        appstack_workload_id: '1',
        appstack_account_id: '1'
      }
    end

    let(:appstack_facts) { appstack_facts }
    let(:facts) { appstack_facts }
  end
end

shared_context :appstack_facts do
  extend RSpec::AppStackFacts
end
