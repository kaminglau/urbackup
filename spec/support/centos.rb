# common facts for CentOS servers
require 'rspec/core/shared_context'

# we need a module to have a default shared_context, we can't use include_context from RSpec.configure
module RSpec
  # CentOS
  module CentOS
    extend RSpec::Core::SharedContext

    def self.centos_facts
      {
        ipaddress: '192.168.1.1',
        kernel: 'Linux',
        operatingsystem: 'CentOS',
        operatingsystemrelease: '6.3',
        osfamily: 'RedHat',
        architecture: 'x86_64'
      }
    end

    let(:centos_facts) { centos_facts }
    let(:facts) { centos_facts }
  end
end

shared_context :centos do
  extend RSpec::CentOS
end
