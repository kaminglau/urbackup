require 'spec_helper_acceptance'

describe 'urbackup class' do
  describe 'running puppet code' do
    it 'should work with no errors' do
      pp = <<-EOS
        import 'urbackup'
        class { 'urbackup': }
      EOS
      # Run it twice and test for idempotency
      apply_manifest(pp, catch_failures: true)
      expect(apply_manifest(pp, catch_failures: true).exit_code).to be_zero
    end

    describe 'urbackup' do
      it 'should install urbackup' do
        shell('urbackup --version', acceptable_exit_codes: 1)
      end
    end
  end
end
