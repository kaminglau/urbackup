require 'spec_helper'

describe 'urbackup', :type => 'class' do
  context 'On a Debian OS with no package name specified' do
    let :facts do
      {
        :osfamily => 'Debian'
      }
    end
    it { should contain_package('urbackup') }
  end
end
